#!/usr/bin/env python
# coding: utf-8

# In[1003]:


import pandas as pd


# In[1004]:


df = pd.read_csv('data.csv')


# In[1005]:


df.columns = ['name', 'review', 'food_type', 'ranking', 'overall_ranking', 'wifi', 'delivery', 'price_range', 'lat', 'long', 'district']


# In[1006]:


index_to_remove = []


# In[1007]:


for index, row in df.iterrows():
    for i in row:
        if i == "no information" or i == "no informations":
            #df.drop(index=index)
            index_to_remove.append(index)


# In[1008]:


index_to_remove_without_duplicate = list(set(index_to_remove))


# In[1009]:


for index,j in df.iterrows():
    if index in index_to_remove_without_duplicate:
        df.drop(index=index, inplace=True)


# In[1010]:


new_list = []


# In[1011]:


for i in df["review"]:
    numeric_filter = filter(str.isdigit, i) #takes all digits into an array
    numeric_string = "".join(numeric_filter) #joins all array elements into a string without separators
    if numeric_string != "": #if 
        numeric_result = int(numeric_string) #converts the string into a int
    else:
        numeric_result = 0
    new_list.append(numeric_result) #adds it to new_list


# In[1012]:


df['review'] = new_list


# In[1013]:


for index, row in df['price_range'].items():
    tmp = row.split()
    if (not (tmp[0]).isdigit()):
        df.drop(index=index, inplace=True)


# In[1014]:


df['price_range']


# In[1015]:


list_mean = []
price_min = []
price_max = []


# In[1016]:


for row in df['price_range']:
    tmp = row.split()
    mean = (int(tmp[0]) + int(tmp[2][1:]))/2
    list_mean.append(mean)
    price_min.append(tmp[0])
    price_max.append(tmp[2][1:])


# In[1017]:


df['average_price'] = list_mean
df['price_min'] = price_min
df['price_max'] = price_max


# In[1018]:


df.head(10)


# In[1019]:


df.drop('price_range', 1, inplace=True)


# In[1020]:


for index, row in df['district'].items():
    tmp = row.split()
    if len(tmp) > 2:
        df.drop(index=index, inplace=True)


# In[1021]:


df.drop('district', 1, inplace=True)


# In[1022]:


list_rank = []


# In[1023]:


for row in df['ranking']:
    tmp = row.split()
    i = tmp[0:3]
    numeric_filter = filter(str.isdigit, i) #takes all digits into an array
    numeric_string = "".join(numeric_filter) #joins all array elements into a string without separators
    if numeric_string != "": #if 
        numeric_result = int(numeric_string) #converts the string into a int
        list_rank.append(numeric_result)


# In[1024]:


for i in range(len(list_rank)):
    if (i != len(list_rank) - 1) and ((list_rank[i] + 1) != list_rank[i+1]):
        list_rank[i+1] = list_rank[i] + 1


# In[1025]:


df['ranking'] = list_rank


# In[1026]:


df.head(10)


# In[1027]:


df.index = df['ranking']


# In[1028]:


wifi_list = []


# In[1029]:


for row in df['wifi']:
    if row == "yes":
        wifi_list.append(True)
    elif row == "no":
        wifi_list.append(False)


# In[1030]:


df['wifi'] = wifi_list


# In[1031]:


delivery_list = []


# In[1032]:


for row in df['delivery']:
    if row == "yes":
        delivery_list.append(True)
    elif row == "no":
        delivery_list.append(False)


# In[1033]:


df['delivery'] = delivery_list


# In[1034]:


df.drop('ranking', 1, inplace=True)


# In[1035]:


ranking_float = []


# In[1036]:


for i in df['overall_ranking']:
    ranking_float.append(float(i))


# In[1037]:


df['overall_ranking'] = ranking_float


# In[1038]:


df['overall_ranking'].mean()


# In[1039]:


import matplotlib.pyplot as plt


# In[1040]:


for index, i in df['review'].items():
    if (i > 5000):
        print(index)


# In[1041]:


df.loc[169]


# In[1042]:


df.head()


# In[1043]:


df['ranking'] = df.index


# In[1044]:


df_ml = df.copy()


# In[1045]:


df_ml.drop('name', 1, inplace=True)


# In[1046]:


df_ml.drop('food_type', 1, inplace=True)


# In[1047]:


df_ml.drop('wifi', 1, inplace=True)


# In[1048]:


df_ml.drop('delivery', 1, inplace=True)


# In[1049]:


df_ml.drop('lat', 1, inplace=True)


# In[1050]:


df_ml.drop('long', 1, inplace=True)


# In[1051]:


df_ml.drop('price_min', 1, inplace=True)


# In[1052]:


df_ml.drop('price_max', 1, inplace=True)


# In[1053]:


df_ml.drop('ranking', 1, inplace=True)


# In[1054]:


df_ml.head(20)


# In[1055]:


from sklearn.linear_model import LinearRegression


# In[1056]:


from sklearn.model_selection import train_test_split


# In[1057]:


y = df_ml['average_price'].values


# In[1058]:


type(y)


# In[1059]:


y.shape


# In[1060]:


x = df_ml.drop('average_price', 1).values


# In[1061]:


x.shape


# In[1062]:


X_train, X_test, y_train, y_test = train_test_split(x, y, test_size = 0.2, random_state=42)


# In[1063]:


reg = LinearRegression()


# In[1064]:


reg.fit(X_train, y_train)


# In[1065]:


import numpy as np


# In[1066]:


from sklearn.metrics import mean_squared_error


# In[1067]:


y_pred = reg.predict(X_test)


# In[1068]:


print("R^2: {}".format(reg.score(X_test, y_test)))
rmse = np.sqrt(mean_squared_error(y_test, y_pred))
print("Root Mean Squared Error: {}".format(rmse))


# In[1069]:


df_ml['PREDICTION'] = reg.predict(x)


# In[1070]:


df_ml['%_error'] = (abs(df_ml['average_price'] - df_ml['PREDICTION']))/df_ml['average_price']*100


# In[1071]:


df_ml.head(20)


# In[ ]:




