#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd


# In[2]:


df = pd.read_csv('data.csv')


# In[3]:


df.columns = ['name', 'review', 'food_type', 'ranking', 'overall_ranking', 'wifi', 'delivery', 'price_range', 'lat', 'long', 'district']


# In[4]:


index_to_remove = []


# In[5]:


for index, row in df.iterrows():
    for i in row:
        if i == "no information" or i == "no informations":
            #df.drop(index=index)
            index_to_remove.append(index)


# In[6]:


index_to_remove_without_duplicate = list(set(index_to_remove))


# In[7]:


for index,j in df.iterrows():
    if index in index_to_remove_without_duplicate:
        df.drop(index=index, inplace=True)


# In[8]:


new_list = []


# In[9]:


for i in df["review"]:
    numeric_filter = filter(str.isdigit, i) #takes all digits into an array
    numeric_string = "".join(numeric_filter) #joins all array elements into a string without separators
    if numeric_string != "": #if 
        numeric_result = int(numeric_string) #converts the string into a int
    else:
        numeric_result = 0
    new_list.append(numeric_result) #adds it to new_list


# In[10]:


df['review'] = new_list


# In[11]:


for index, row in df['price_range'].items():
    tmp = row.split()
    if (not (tmp[0]).isdigit()):
        df.drop(index=index, inplace=True)


# In[12]:


df['price_range']


# In[13]:


list_mean = []
price_min = []
price_max = []


# In[14]:


for row in df['price_range']:
    tmp = row.split()
    mean = (int(tmp[0]) + int(tmp[2][1:]))/2
    list_mean.append(mean)
    price_min.append(tmp[0])
    price_max.append(tmp[2][1:])


# In[15]:


df['average_price'] = list_mean
df['price_min'] = price_min
df['price_max'] = price_max


# In[16]:


df.head(10)


# In[17]:


df.drop('price_range', 1, inplace=True)


# In[18]:


for index, row in df['district'].items():
    tmp = row.split()
    if len(tmp) > 2:
        df.drop(index=index, inplace=True)


# In[19]:


df.drop('district', 1, inplace=True)


# In[20]:


list_rank = []


# In[21]:


for row in df['ranking']:
    tmp = row.split()
    i = tmp[0:3]
    numeric_filter = filter(str.isdigit, i) #takes all digits into an array
    numeric_string = "".join(numeric_filter) #joins all array elements into a string without separators
    if numeric_string != "": #if 
        numeric_result = int(numeric_string) #converts the string into a int
        list_rank.append(numeric_result)


# In[22]:


for i in range(len(list_rank)):
    if (i != len(list_rank) - 1) and ((list_rank[i] + 1) != list_rank[i+1]):
        list_rank[i+1] = list_rank[i] + 1


# In[23]:


df['ranking'] = list_rank


# In[24]:


df.head(10)


# In[25]:


df.index = df['ranking']


# In[26]:


wifi_list = []


# In[27]:


for row in df['wifi']:
    if row == "yes":
        wifi_list.append(True)
    elif row == "no":
        wifi_list.append(False)


# In[28]:


df['wifi'] = wifi_list


# In[29]:


delivery_list = []


# In[30]:


for row in df['delivery']:
    if row == "yes":
        delivery_list.append(True)
    elif row == "no":
        delivery_list.append(False)


# In[73]:


df['delivery'] = delivery_list


# In[33]:


df.drop('ranking', 1, inplace=True)


# In[61]:


ranking_float = []


# In[62]:


for i in df['overall_ranking']:
    ranking_float.append(float(i))


# In[63]:


df['overall_ranking'] = ranking_float


# In[64]:


df['overall_ranking'].mean()


# In[65]:


import matplotlib.pyplot as plt


# In[67]:


for index, i in df['review'].items():
    if (i > 5000):
        print(index)


# In[72]:


df.loc[169]


# In[74]:


df.head()


# In[ ]:




