# Projet BA08

## 15/11/2021

1. [DONE] nettoyer la colonne review
2. [DONE] essayer de calculer la moyenne de la colonne average price
3. [DONE] supprimer toutes les lignes qui ont un attribut 'no informations'
4. [DONE] récupérer que le numéro dans ranking + virer les autres villes en utilisant district
5. [ABANDONNÉ] checker synonymes foodtypes

## 22/11/2021

6. [DONE] revoir le machine learning
7. préparer des modèles (clustering, classification, régression)
8. affichages
9. commentaires

## 29/11/2021

10. avoir un r² satisfaisant!!!!!!
